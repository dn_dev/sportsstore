﻿CREATE TABLE [dbo].[Product] (
    [ProductId]     INT             IDENTITY (1, 1) NOT NULL,
    [Price]         DECIMAL (18)    NULL,
    [Name]          VARCHAR (50)    NULL,
    [Description]   VARCHAR (50)    NULL,
    [Category]      VARCHAR (50)    NULL,
    [ImageData]     VARBINARY (MAX) NULL,
    [ImageMimeType] VARCHAR (50)    NULL,
    CONSTRAINT [PK_Product_ProductId] PRIMARY KEY CLUSTERED ([ProductId] ASC)
);



