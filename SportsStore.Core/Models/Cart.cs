﻿using System.Collections.Generic;
using System.Linq;

namespace SportsStore.Core.Models
{
    public class Cart
    {
        private readonly List<CartLine> _cartLineList = new List<CartLine>();

        public void AddItem(Product product, int quantity)
        {
            var cartLine = _cartLineList.FirstOrDefault(x => x.Product.ProductId == product.ProductId);
            if (cartLine == null)
            {
                _cartLineList.Add(new CartLine()
                {
                    Product = product,
                    Qty = quantity
                });
            }
            else
            {
                cartLine.Qty += quantity;
            }
        }

        public void RemoveLine(Product product)
        {
            _cartLineList.RemoveAll(x => x.Product.ProductId == product.ProductId);
        }

        public decimal ComputeTotalValue()
        {
            return _cartLineList.Sum(x => x.Product.Price * x.Qty);
        }

        public void Clear()
        {
            _cartLineList.Clear();
        }

        public IEnumerable<CartLine> Lines => _cartLineList;

        public class CartLine
        {
            public Product Product { get; set; }
            public int Qty { get; set; }
        }
    }
}
