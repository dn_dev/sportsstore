﻿using SportsStore.Core.Models;

namespace SportsStore.Core.Interfaces.Repositories
{
    public interface IProductRepository : IRepositoryBase<Product>
    {
        Product Get(int id);
    }
}
