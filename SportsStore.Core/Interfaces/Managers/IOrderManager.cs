﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsStore.Core.Models;

namespace SportsStore.Core.Interfaces.Managers
{
    public interface IOrderManager
    {
        void ProcessOrder(Cart cart, ShippingDetails shippingDetails);
    }
}
