﻿using SportsStore.Core.Models;

namespace SportsStore.Core.Interfaces.Managers
{
    public interface IProductManager
    {
        Product[] GetAllProducts();

        Product GetProductById(int productId);

        void AddProduct(string name, string description, string category, decimal price, string imageMimeType, byte[] imageData);

        void EditProduct(int productId, string name, string description, string category, decimal price, string imageMimeType, byte[] imageData);

        void DeleteProduct(int productId);
    }
}
