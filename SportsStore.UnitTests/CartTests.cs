﻿using System;
using System.Linq;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportsStore.Business.Managers;
using SportsStore.Core.Interfaces.Managers;
using SportsStore.Core.Models;
using SportsStore.Data.EmailSettings;
using SportsStore.WebUI.Controllers;
using SportsStore.WebUI.Models.Cart;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class CartTests
    {
        [TestMethod]
        public void CanAddNewLines()
        {
            // arrange
            Product p1 = new Product() {ProductId = 1, Name = "P1"};
            Product p2 = new Product() {ProductId = 2, Name = "P2"};
            Cart target = new Cart();
            // act
            target.AddItem(p1,1);
            target.AddItem(p1,1);
            target.AddItem(p2,2);
            Cart.CartLine[] results = target.Lines.ToArray();
            // assert
            Assert.AreEqual(results.Length,2);
            Assert.AreEqual(results[1].Product,p2);
            Assert.AreEqual(results[0].Qty,2);
        }

        [TestMethod]
        public void CanAddToCart()
        {
            // arrange
            Mock<IProductManager> mock = new Mock<IProductManager>();
            mock.Setup(x => x.GetAllProducts()).Returns(new Product[]
            {
                new Product
                {
                    ProductId = 1,
                    Category = "Watersports",
                    Name = "Kayak",
                    Description = "A boat for one person",
                    Price = 275
                },
                new Product
                {
                    ProductId = 2,
                    Category = "Watersports",
                    Name = "LifeJacket",
                    Description = "Protective and fashionable",
                    Price = 49
                },
                new Product
                {
                    ProductId = 3,
                    Category = "Soccer",
                    Name = "Soccer Ball",
                    Description = "FIFA-Aproved size and weight",
                    Price = 20
                },
                new Product
                {
                    ProductId = 4,
                    Category = "Soccer",
                    Name = "Corner Flags",
                    Description = "Give your playing field a proffesional touch",
                    Price = 35
                },
                new Product
                {
                    ProductId = 5,
                    Category = "Soccer",
                    Name = "Stadium",
                    Description = "Flat-packed, 35,000-seat stadium",
                    Price = 79500
                },
                new Product
                {
                    ProductId = 6,
                    Category = "Chess",
                    Name = "Thinking Cap",
                    Description = "Improve your brain efficiency",
                    Price = 16
                },
                new Product
                {
                    ProductId = 7,
                    Name = "Unsteady Chair",
                    Description = "Secretary give your opponent a disadvantage",
                    Price = 30,
                    Category = "Chess"

                },
                new Product
                {
                    ProductId = 8,
                    Name = "Human Chess Board",
                    Description = "A fun game for the family",
                    Category = "Chess",
                    Price = 75
                },
                new Product
                {
                    ProductId = 9,
                    Name = "Bling-Bling King",
                    Description = "Gold-plated, diamond-student King",
                    Price = 1200,
                    Category = "Chess"
                }
            });
            var cart = new Cart();
            var orderManager = new OrderManager(new EmailSettings());
            var target = new CartController(mock.Object, orderManager);
            // action
            target.AddToCart(cart, 1, null);
            // assert
            Assert.AreEqual(cart.Lines.Count(),1);
            Assert.AreEqual(cart.Lines.ToArray()[0].Product.ProductId,1);
        }

        [TestMethod]
        public void AddingProductToCartGoesToCartScreen()
        {
            var orderManager = new OrderManager(new EmailSettings());
            Mock<IProductManager> mock = new Mock<IProductManager>();
            mock.Setup(x => x.GetAllProducts()).Returns(new Product[]
            {
                new Product
                {
                    ProductId = 1,
                    Category = "Watersports",
                    Name = "Kayak",
                    Description = "A boat for one person",
                    Price = 275
                },
                new Product
                {
                    ProductId = 2,
                    Category = "Watersports",
                    Name = "LifeJacket",
                    Description = "Protective and fashionable",
                    Price = 49
                },
                new Product
                {
                    ProductId = 3,
                    Category = "Soccer",
                    Name = "Soccer Ball",
                    Description = "FIFA-Aproved size and weight",
                    Price = 20
                },
                new Product
                {
                    ProductId = 4,
                    Category = "Soccer",
                    Name = "Corner Flags",
                    Description = "Give your playing field a proffesional touch",
                    Price = 35
                },
                new Product
                {
                    ProductId = 5,
                    Category = "Soccer",
                    Name = "Stadium",
                    Description = "Flat-packed, 35,000-seat stadium",
                    Price = 79500
                },
                new Product
                {
                    ProductId = 6,
                    Category = "Chess",
                    Name = "Thinking Cap",
                    Description = "Improve your brain efficiency",
                    Price = 16
                },
                new Product
                {
                    ProductId = 7,
                    Name = "Unsteady Chair",
                    Description = "Secretary give your opponent a disadvantage",
                    Price = 30,
                    Category = "Chess"

                },
                new Product
                {
                    ProductId = 8,
                    Name = "Human Chess Board",
                    Description = "A fun game for the family",
                    Category = "Chess",
                    Price = 75
                },
                new Product
                {
                    ProductId = 9,
                    Name = "Bling-Bling King",
                    Description = "Gold-plated, diamond-student King",
                    Price = 1200,
                    Category = "Chess"
                }
            });
            var cart = new Cart();
            var target = new CartController(mock.Object, orderManager);
            var result = target.AddToCart(cart, 2, "myUrl");
            // assert
            Assert.AreEqual((string) result.RouteValues["action"],"Index");
            Assert.AreEqual((string) result.RouteValues["returnUrl"],"myUrl");
        }

        [TestMethod]
        public void CanViewCartContents()
        {
            // arrange
            var cart = new Cart();
            var target = new CartController();
            // action
            var result = (CartIndexViewModel) target.Index(cart, "myUrl").ViewData.Model;
            // assert
            Assert.AreSame(result.Cart,cart);
            Assert.AreEqual(result.ReturnUrl,"myUrl");
        }
    }
}
