﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportsStore.Core.Interfaces.Managers;
using SportsStore.Core.Models;
using SportsStore.WebUI.Controllers;
using SportsStore.WebUI.Models.Product;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class AdminTest
    {
        [TestMethod]
        public void Can_Edit_Product()
        {
            //Arrange
            Mock<IProductManager> mock = new Mock<IProductManager>();
            mock.Setup(m => m.GetAllProducts()).Returns(new Product[]
            {
                new Product
                {
                    ProductId = 1,
                    Category = "Watersports",
                    Name = "Kayak",
                    Description = "A boat for one person",
                    Price = 275
                },
                new Product
                {
                    ProductId = 2,
                    Category = "Watersports",
                    Name = "LifeJacket",
                    Description = "Protective and fashionable",
                    Price = 49
                },
                new Product
                {
                    ProductId = 3,
                    Category = "Soccer",
                    Name = "Soccer Ball",
                    Description = "FIFA-Aproved size and weight",
                    Price = 20
                },
                new Product
                {
                    ProductId = 4,
                    Category = "Soccer",
                    Name = "Corner Flags",
                    Description = "Give your playing field a proffesional touch",
                    Price = 35
                },
                new Product
                {
                    ProductId = 5,
                    Category = "Soccer",
                    Name = "Stadium",
                    Description = "Flat-packed, 35,000-seat stadium",
                    Price = 79500
                },
                new Product
                {
                    ProductId = 6,
                    Category = "Chess",
                    Name = "Thinking Cap",
                    Description = "Improve your brain efficiency",
                    Price = 16
                },
                new Product
                {
                    ProductId = 7,
                    Name = "Unsteady Chair",
                    Description = "Secretary give your opponent a disadvantage",
                    Price = 30,
                    Category = "Chess"

                },
                new Product
                {
                    ProductId = 8,
                    Name = "Human Chess Board",
                    Description = "A fun game for the family",
                    Category = "Chess",
                    Price = 75
                },
                new Product
                {
                    ProductId = 9,
                    Name = "Bling-Bling King",
                    Description = "Gold-plated, diamond-student King",
                    Price = 1200,
                    Category = "Chess"
                }
            });
            AdminController target = new AdminController(mock.Object);
            var productModel = new ProductModel() {ProductId = 1};
            //Action
            target.DeleteProduct(productModel);
            //Assert
            mock.Verify(m => m.DeleteProduct(productModel.ProductId));
        }
    }
}
