﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportsStore.Business.Managers;
using SportsStore.Core.Interfaces.Managers;
using SportsStore.Core.Interfaces.Repositories;
using SportsStore.Core.Interfaces.UnitOfWork;
using SportsStore.Core.Models;
using SportsStore.WebUI.Controllers;
using SportsStore.WebUI.Models.PagingInfoModel;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        private IProductManager _manager;
        private Product[] _products;
        public UnitTest1()
        {

        }
        public UnitTest1(IProductManager manager)
        {
            _manager = manager;
            _products = _manager.GetAllProducts();
        }
        [TestMethod]
        public void CanSendPaginationViewModel()
        {
            // Arrange
           //Mock<IProductRepository> mock2 = new Mock<IProductRepository>();
           var mock = new Mock<IProductManager>();
            mock.Setup(m => m.GetProductById(1)).Returns(

                    new Product()
                    {
                        ProductId = 1,
                        Name = "Kayak",
                        Description = "A boat for one person",
                        Category = "Watersports",
                        Price = 275
                    }

            );

        }
        [TestMethod]
        //[ExpectedException(typeof(System.ArgumentNullException))]
        public void CanSendPagination2()
        {
            var mock2 = new Mock<IProductManager>();
            mock2.Setup(m => m.GetAllProducts()).Returns(_products);
            //// Arrange
            ProductController controller = new ProductController(mock2.Object);
            //controller.pageSize = 3;
            //Assert.AreEqual(controller.pageSize,3);
        }

        [TestMethod]
        public void CanFilterProducts()
        {
            Mock<IProductManager> mock = new Mock<IProductManager>();
            mock.Setup(m => m.GetAllProducts()).Returns(new Product[]
            {
                new Product
                {
                    ProductId = 1,
                    Category = "Watersports",
                    Name = "Kayak",
                    Description = "A boat for one person",
                    Price = 275
                },
                new Product
                {
                    ProductId = 2,
                    Category = "Watersports",
                    Name = "LifeJacket",
                    Description = "Protective and fashionable",
                    Price = 49
                },
                new Product
                {
                    ProductId = 3,
                    Category = "Soccer",
                    Name = "Soccer Ball",
                    Description = "FIFA-Aproved size and weight",
                    Price = 20
                },
                new Product
                {
                    ProductId = 4,
                    Category = "Soccer",
                    Name = "Corner Flags",
                    Description = "Give your playing field a proffesional touch",
                    Price = 35
                },
                new Product
                {
                    ProductId = 5,
                    Category = "Soccer",
                    Name = "Stadium",
                    Description = "Flat-packed, 35,000-seat stadium",
                    Price = 79500
                },
                new Product
                {
                    ProductId = 6,
                    Category = "Chess",
                    Name = "Thinking Cap",
                    Description = "Improve your brain efficiency",
                    Price = 16
                },
                new Product
                {
                    ProductId = 7,
                    Name = "Unsteady Chair",
                    Description = "Secretary give your opponent a disadvantage",
                    Price = 30,
                    Category = "Chess"

                },
                new Product
                {
                    ProductId = 8,
                    Name = "Human Chess Board",
                    Description = "A fun game for the family",
                    Category = "Chess",
                    Price = 75
                },
                new Product
                {
                    ProductId = 9,
                    Name = "Bling-Bling King",
                    Description = "Gold-plated, diamond-student King",
                    Price = 1200,
                    Category = "Chess"
                }
            });
            ProductController controller = new ProductController(mock.Object);
            Product[] result = ((ProductListModel) controller.List("Watersports").Model).Products.ToArray();
            Assert.AreEqual(result.Length,2);
            Assert.AreEqual(result[0].Category,"Watersports");
            Assert.IsTrue(result[0].Name == "Kayak");
        }
    }
}
