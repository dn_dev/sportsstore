﻿using System.Data.Entity.ModelConfiguration;
using SportsStore.Core.Models;

namespace SportsStore.Data.Mapping
{
    public class ProductMap : EntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            // Primary Key
            this.HasKey(x => x.ProductId);

            // Properties

            // Table & Column Mappings
            this.ToTable("Product");

            // Navigation properties

            //this.HasMany(t => t.);
        }
    }
}
