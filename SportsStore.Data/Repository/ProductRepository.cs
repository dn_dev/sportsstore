﻿using System.Linq;
using SportsStore.Core.Interfaces.Repositories;
using SportsStore.Core.Interfaces.UnitOfWork;
using SportsStore.Core.Models;

namespace SportsStore.Data.Repository
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Product Get(int id)
        {
            return this.Select(x => x.ProductId == id).FirstOrDefault();
        }
    }
}
