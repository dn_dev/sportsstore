﻿using System.Linq;
using System.Web.Mvc;
using SportsStore.Core.Interfaces.Managers;
using SportsStore.WebUI.Models.PagingInfoModel;

namespace SportsStore.WebUI.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductManager _productManager;
        private int _pageSize = 4;
        public ProductController(IProductManager productManager)
        {
            _productManager = productManager;
        }

        public ViewResult List(string category, int page = 1)
        {
            var productListModel = new ProductListModel()
            {
                Products = _productManager.GetAllProducts().Where(x => category == null || x.Category == category).
                OrderBy(x => x.ProductId).Skip((page-1)*_pageSize).Take(_pageSize).ToArray(),

                PagingInfoModel = new PagingInfoModel()
                {
                    TotalItems = (category == null) ? _productManager.GetAllProducts().Count() :
                    _productManager.GetAllProducts().Count(x => x.Category == category),
                    CurrentPage = page,
                    ItemsPerPage = _pageSize
                },

                CurrentCategory = category
            };
            return View("ProductList",productListModel);
        }

        public FileContentResult GetImage(int productId)
        {
            var product = _productManager.GetProductById(productId);
            if (product != null)
            {
                return File(product.ImageData, product.ImageMimeType);
            }
            return null;
        }

    }
}