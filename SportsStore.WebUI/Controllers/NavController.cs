﻿using System.Linq;
using System.Web.Mvc;
using SportsStore.Core.Interfaces.Managers;

namespace SportsStore.WebUI.Controllers
{
    public class NavController : Controller
    {
        private IProductManager _productManager;

        public NavController(IProductManager productManager)
        {
            _productManager = productManager;
        }

        public PartialViewResult Menu(string category = null)
        {
            ViewBag.SelectedCategory = category;
            var categoryList = _productManager.GetAllProducts().Select(x => x.Category).Distinct().OrderBy(x => x);
            return PartialView("FlexMenu", categoryList);
        }
    }
}