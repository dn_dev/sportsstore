﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore.Core.Interfaces.Managers;
using SportsStore.WebUI.Infrastructure;
using SportsStore.WebUI.Models.Product;

namespace SportsStore.WebUI.Controllers
{
    public class AdminController : Controller
    {
        private readonly IProductManager _productManager;

        public AdminController(IProductManager productManager)
        {
            _productManager = productManager;
        }
        // GET: Admin
        //[GmailAuth]
        [Authorize(Users = "admin@gmail.com")]
        public ActionResult Index()
        {
            var productsArray = _productManager.GetAllProducts();
            return View(productsArray);
        }
        [HttpGet]
        public ActionResult AddNewProduct()
        {
            return View("ProductDetails", new ProductModel());
        }

        [HttpPost]
        public ActionResult AddNewProduct(ProductModel product)
        {
            ValidateProduct(product);

            if (ModelState.IsValid)
            {
                _productManager.AddProduct(product.Name,product.Description,product.Description,product.Price,product.ImageMimeType,product.ImageData);
                return RedirectToAction("Index");
            }
            return View("ProductDetails", product);
        }
        [HttpGet]
        public ActionResult EditProduct(int productId)
        {
            var model = GetProductModel(productId);
            return View("ProductDetails", model);
        }

        [HttpPost]
        public ActionResult EditProduct(ProductModel product, HttpPostedFileBase image = null)
        {
            ValidateProduct(product);
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    product.ImageMimeType = image.ContentType;
                    product.ImageData = new byte[image.ContentLength];
                    image.InputStream.Read(product.ImageData, 0, image.ContentLength);
                }
                _productManager.EditProduct(product.ProductId, product.Name, product.Description, product.Description,
                    product.Price, product.ImageMimeType, product.ImageData);
                TempData["message"] = $"{product.Name} has been saved";
                return RedirectToAction("Index");
            }

            return View("ProductDetails",product);
        }

        [HttpPost]
        public ActionResult DeleteProduct(ProductModel product)
        {
            _productManager.DeleteProduct(product.ProductId);
            return RedirectToAction("Index");
        }

        private void ValidateProduct(ProductModel product)
        {
            product.Category = product.Category.Trim();
            product.Description = product.Description.Trim();
            product.Name = product.Name.Trim();
            product.Name = product.Name.Replace(product.Name[0], char.ToUpper(product.Name[0]));
        }

        private ProductModel GetProductModel(int productId)
        {
            var product = _productManager.GetProductById(productId);
            var model = new ProductModel()
            {
                ProductId = product.ProductId,
                Description = product.Description,
                Name = product.Name,
                Price = product.Price,
                Category = product.Category,
                ImageData = product.ImageData,
                ImageMimeType = product.ImageMimeType
            };
            return model;
        }

    }
}