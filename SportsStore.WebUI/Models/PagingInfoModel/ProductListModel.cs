﻿using SportsStore.Core.Models;

namespace SportsStore.WebUI.Models.PagingInfoModel
{
    public class ProductListModel
    {
        public Core.Models.Product[] Products { get; set; }
        public PagingInfoModel PagingInfoModel { get; set; }
        public string CurrentCategory { get; set; }
    }
}