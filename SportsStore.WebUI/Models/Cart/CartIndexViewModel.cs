﻿using SportsStore.Core.Models;
namespace SportsStore.WebUI.Models.Cart
{
    public class CartIndexViewModel
    {
        public Core.Models.Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}