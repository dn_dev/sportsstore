﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;
using Microsoft.AspNet.Identity;

namespace SportsStore.WebUI.Infrastructure
{
    public class GmailAuthAttribute : FilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)

        {
            var ident = filterContext.Principal.Identity;
            if (!ident.IsAuthenticated || !ident.Name.EndsWith("gmail.com"))
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.Result == null || filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary()
                {
                    {"controller","Account"},
                    {"action","Login"},
                    {"returnUrl",filterContext.HttpContext.Request.RawUrl }
                });
            }
            else
            {
                //filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary()
                //{
                //    {"controller","Account"},
                //    {"action","LogOff"},
                //    {"returnUrl",filterContext.HttpContext.Request.RawUrl }
                //});
                filterContext.HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            }
        }
    }
}